unit class Chinese::Tools:ver<0.0.1>;

use Grammar::Debugger;

our %pinyin-initials = %(
    (<b p m f d t n l g k h j q x zh ch sh r z c s> Z ('ㄅ'…'ㄙ')).flat
);

say %pinyin-initials;

our %zhuyin-initials = %pinyin-initials.antipairs;

say %zhuyin-initials;

our %pinyin-medials = %(
    (<i u ü> Z <ㄧ ㄨ ㄩ>).flat
);

our %zhuyin-medials = %pinyin-medials.antipairs;

our %pinyin-rhymes = %(
    (<a o e ê ai ei ao ou an en ang eng er> Z ('ㄚ'…'ㄦ')).flat
);

our %zhuyin-rhymes = %pinyin-rhymes.antipairs;

our %pinyin-tones = %(
    (1..5 Z «' ' ˊ ˇ ˋ ˙»).flat
);

our %zhuyin-tones = %pinyin-tones.antipairs;

grammar Test {
    token TOP { « @( %pinyin-initials.keys ) » }
}

grammar Pinyin::To::Zhuyin {
    token TOP      { <word>+ %% \h+ }
    token word     { <syllable>+ }
    token syllable { <initial>? <medial>? <rhyme>? <tone>? }
    token initial  { « @( %pinyin-initials.keys ) » }
    token medial   { « @( %pinyin-medials.keys ) » }
    token rhyme    { « @( %pinyin-rhymes.keys ) » }
    token tone     { <[1..5]> }
}

grammar Zhuyin::To::Pinyin {
    token TOP      { <word>+ %% \h+ }
    token word     { <syllable>+ }
    token syllable {
        | <initial> <medial> <rhyme> <tone>?
        | <initial> <medial> <tone>?
        | <initial> <rhyme> <tone>?
        | <medial> <rhyme> <tone>?
        | <medial> <tone>?
        | <rhyme> <tone>?
    }
    token initial  { « @( %zhuyin-initials.keys ) » }
    token medial   { « @( %zhuyin-medials.keys ) » }
    token rhyme    { « @( %zhuyin-rhymes.keys ) » }
    token tone     { <[ˊˇˋ˙]> }
}
#say ::('%zhuyin-initials');

=begin comment
my $test = 'w';
my $testout = Test.parse($test) or die "$!";
say $testout;

my $pinyin = "wo3 shi4";
my $zhuyin = Pinyin::To::Zhuyin.parse($pinyin) or die "$!";
say $zhuyin;
=end comment

my $zhuyin = 'ㄋㄧˇ';
my $pinyin = Zhuyin::To::Pinyin.parse($zhuyin) or die "$!";

say $pinyin;


=begin pod

=head1 NAME

Chinese::Tools - blah blah blah

=head1 SYNOPSIS

=begin code :lang<raku>

use Chinese::Tools;

=end code

=head1 DESCRIPTION

Chinese::Tools is ...

=head1 AUTHOR

Perry Thompson <contact@ryper.org>

=head1 COPYRIGHT AND LICENSE

Copyright 2020 Perry Thompson

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

=end pod
